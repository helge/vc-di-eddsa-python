import bovine

from vc_di_eddsa import EdDSA_2022

secret = "z3u2Yxcowsarethebestcowsarethebestcowsarethebest"


def test_c390():
    did_key = bovine.crypto.private_key_to_did_key(secret)
    created = "2023-04-25T00:00:00Z"

    doc = {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://www.w3.org/ns/did/v1",
            "https://www.w3.org/ns/credentials/v2",
            {
                "fep": "https://w3id.org/fep#",
                "VerifiableIdentityStatement": "fep:VerifiableIdentityStatement",
                "subject": "fep:subject",
            },
        ],
        "id": "https://mymath.rocks/moocowexample#mookey",
        "type": "VerifiableIdentityStatement",
        "subject": did_key,
        "alsoKnownAs": "https://mymath.rocks/moocowexample",
    }

    eddsa = EdDSA_2022(doc, did_key, created)

    result = eddsa.signed_doc(secret)

    expected = {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://www.w3.org/ns/did/v1",
            "https://www.w3.org/ns/credentials/v2",
            {
                "VerifiableIdentityStatement": "fep:VerifiableIdentityStatement",
                "fep": "https://w3id.org/fep#",
                "subject": "fep:subject",
            },
        ],
        "alsoKnownAs": "https://mymath.rocks/moocowexample",
        "id": "https://mymath.rocks/moocowexample#mookey",
        "proof": {
            "created": "2023-04-25T00:00:00Z",
            "cryptosuite": "json-eddsa-2022",
            "proofPurpose": "assertionMethod",
            "type": "DataIntegrityProof",
            "verificationMethod": "did:key:z6MkekwC6R9bj9ErToB7AiZJfyCSDhaZe1UxhDbCqJrhqpS5",
            "proofValue": "z5SuCWVAEbM2LmAwYF9YqyybqpbmmG7R7944L9NmmpMTMKxk8FT1F9CcKApyvbwair21UeR3aMLncFBV5fwwPbXyk",
        },
        "subject": "did:key:z6MkekwC6R9bj9ErToB7AiZJfyCSDhaZe1UxhDbCqJrhqpS5",
        "type": "VerifiableIdentityStatement",
    }

    assert result == expected


def test_c390_no_context():
    did_key = bovine.crypto.private_key_to_did_key(secret)
    created = "2023-04-25T00:00:00Z"

    doc = {
        "id": "https://mymath.rocks/moocowexample#mookey",
        "type": "VerifiableIdentityStatement",
        "subject": did_key,
        "alsoKnownAs": "https://mymath.rocks/moocowexample",
    }

    eddsa = EdDSA_2022(doc, did_key, created)

    result = eddsa.signed_doc(secret)

    expected = {
        "alsoKnownAs": "https://mymath.rocks/moocowexample",
        "id": "https://mymath.rocks/moocowexample#mookey",
        "proof": {
            "created": "2023-04-25T00:00:00Z",
            "cryptosuite": "json-eddsa-2022",
            "proofPurpose": "assertionMethod",
            "type": "DataIntegrityProof",
            "verificationMethod": "did:key:z6MkekwC6R9bj9ErToB7AiZJfyCSDhaZe1UxhDbCqJrhqpS5",
            "proofValue": "z3XrkXxuHTfH8WxEbQnsKnTkkAc27NUunKyyvGJzmQknbk5NCyd8JQ9eSsUge7vnDWaPkgeRC9xRLbwPvVRaFLc3r",
        },
        "subject": "did:key:z6MkekwC6R9bj9ErToB7AiZJfyCSDhaZe1UxhDbCqJrhqpS5",
        "type": "VerifiableIdentityStatement",
    }

    assert result == expected
