from dataclasses import dataclass
import jcs
import requests_cache
from pyld import jsonld
import hashlib
from multiformats import multibase, multicodec

from cryptography.hazmat.primitives.asymmetric import ed25519

requests_cache.install_cache("context_cache")


example_context = {
    "BachelorDegree": "https://www.w3.org/ns/credentials/examples#BachelorDegree",
    "UniversityDegreeCredential": "https://www.w3.org/ns/credentials/examples#UniversityDegreeCredential",  # noqa F501
    "AlumniCredential": "https://www.w3.org/ns/credentials/examples#AlumniCredential",
    "alumniOf": "https://schema.org/alumniOf",
    "degree": "https://www.w3.org/ns/credentials/examples#degree",
    "degreeType": "https://www.w3.org/ns/credentials/examples#degreeType",
    "degreeSchool": "https://www.w3.org/ns/credentials/examples#degreeSchool",
    "college": "https://www.w3.org/ns/credentials/examples#college",
    "name": "https://schema.org/name",
    "givenName": "https://schema.org/givenName",
    "familyName": "https://schema.org/familyName",
}


def wrapper(url, options, **kwargs):
    if url == "https://www.w3.org/ns/credentials/examples/v2":
        return {
            "contentType": "application/ld+json",
            "contextUrl": None,
            "documentUrl": url,
            "document": {"@context": example_context},
        }

    result = jsonld.requests_document_loader(timeout=60)(url, options)
    return result


jsonld.set_document_loader(wrapper)


@dataclass
class EdDSA_2022:
    doc: dict
    verificationMethod: str
    created: str
    proof: dict = None
    norm_doc: str = None
    cryptosuite: str = "jcs-eddsa-2022"

    def canonicalize(self, doc):
        if self.cryptosuite == "eddsa-2022":
            doc["@context"] = self.doc["@context"]
            return jsonld.normalize(
                doc, {"algorithm": "URDNA2015", "format": "application/n-quads"}
            ).encode("utf-8")

        return jcs.canonicalize(doc)

    @property
    def norm_hash(self):
        if not self.norm_doc:
            self.norm_doc = self.canonicalize(self.doc)

        return hashlib.sha256(self.norm_doc)

    @property
    def proof_prop(self):
        if self.proof:
            return self.proof
        return {
            "created": self.created,
            "cryptosuite": self.cryptosuite,
            "proofPurpose": "assertionMethod",
            "type": "DataIntegrityProof",
            "verificationMethod": self.verificationMethod,
        }

    @property
    def proof_canon(self):
        return self.canonicalize(self.proof_prop)

    @property
    def proof_hash(self):
        return hashlib.sha256(self.proof_canon)

    @property
    def combined(self):
        return self.proof_hash.digest() + self.norm_hash.digest()

    def sign(self, private_key_str):
        decoded = multibase.decode(private_key_str)
        codec, key_bytes = multicodec.unwrap(decoded)
        assert codec.name == "ed25519-priv"

        private_key = ed25519.Ed25519PrivateKey.from_private_bytes(key_bytes)

        return multibase.encode(private_key.sign(self.combined), "base58btc")

    def signed_doc(self, private_key_str):
        result = self.doc.copy()
        new_proof = self.proof_prop.copy()
        new_proof["proofValue"] = self.sign(private_key_str)
        if "@context" in new_proof:
            del new_proof["@context"]
        result["proof"] = new_proof

        return result

    @staticmethod
    def verify(doc, public_key_str):
        decoded = multibase.decode(public_key_str)
        codec, key_bytes = multicodec.unwrap(decoded)
        assert codec.name == "ed25519-pub"

        public_key = ed25519.Ed25519PublicKey.from_public_bytes(key_bytes)

        doc_copy = doc.copy()
        del doc_copy["proof"]

        proof_copy = doc["proof"].copy()

        signature = multibase.decode(proof_copy["proofValue"])
        del proof_copy["proofValue"]

        ed = EdDSA_2022(
            doc_copy,
            proof_copy["verificationMethod"],
            proof_copy["created"],
            proof=proof_copy,
            cryptosuite=proof_copy["cryptosuite"],
        )

        public_key.verify(signature, ed.combined)
