from vc_di_eddsa import EdDSA_2022

doc = {
    "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://www.w3.org/ns/credentials/examples/v2",
    ],
    "id": "urn:uuid:58172aac-d8ba-11ed-83dd-0b3aef56cc33",
    "type": ["VerifiableCredential", "AlumniCredential"],
    "name": "Alumni Credential",
    "description": "An minimum viable example of an Alumni Credential.",
    "issuer": "https://vc.example/issuers/5678",
    "validFrom": "2023-01-01T00:00:00Z",
    "credentialSubject": {
        "id": "did:example:abcdefgh",
        "alumniOf": "The School of Examples",
    },
}


def test_algo():
    eddsa = EdDSA_2022(
        doc,
        "https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2",
        "2023-02-24T23:36:38Z",
        cryptosuite="eddsa-2022",
    )

    assert (
        eddsa.norm_hash.hexdigest()
        == "4d102b37cf914c23856e2fd35a06758d8a48d0493d8d14185b0b2b2b40275008"
    )

    assert (
        eddsa.proof_hash.hexdigest()
        == "23582f019d9bc92491aafed33d89e3754cf6af3f128932ad281d2f9537bb3ee3"
    )

    signed = eddsa.sign("z3u2en7t5LR2WtQH5PfFqMqwVHBeXouLzo6haApm8XHqvjxq")

    assert (
        signed
        == "zytcLynZ8bVqCNnfw9zrGQonkmMgwRVsNiQs5WFUnUxpQsUzjT1tG27Tz9A7x8M4rGgA3nzqdQi3xjvAVhet1vFP"  # noqa F501
    )

    doc_signed = eddsa.signed_doc("z3u2en7t5LR2WtQH5PfFqMqwVHBeXouLzo6haApm8XHqvjxq")

    assert doc_signed == {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://www.w3.org/ns/credentials/examples/v2",
        ],
        "id": "urn:uuid:58172aac-d8ba-11ed-83dd-0b3aef56cc33",
        "type": ["VerifiableCredential", "AlumniCredential"],
        "name": "Alumni Credential",
        "description": "An minimum viable example of an Alumni Credential.",
        "issuer": "https://vc.example/issuers/5678",
        "validFrom": "2023-01-01T00:00:00Z",
        "credentialSubject": {
            "id": "did:example:abcdefgh",
            "alumniOf": "The School of Examples",
        },
        "proof": {
            "type": "DataIntegrityProof",
            "cryptosuite": "eddsa-2022",
            "created": "2023-02-24T23:36:38Z",
            "verificationMethod": "https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2",  # noqa F501
            "proofPurpose": "assertionMethod",
            "proofValue": "zytcLynZ8bVqCNnfw9zrGQonkmMgwRVsNiQs5WFUnUxpQsUzjT1tG27Tz9A7x8M4rGgA3nzqdQi3xjvAVhet1vFP",  # noqa F501
        },
    }

    EdDSA_2022.verify(doc_signed, "z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2")


def test_algo_jcs():
    eddsa = EdDSA_2022(
        doc,
        "https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2",
        "2023-02-24T23:36:38Z",
    )

    assert (
        eddsa.norm_hash.hexdigest()
        == "d3e49ee249b3382558a7f0bcc4a900e2fa6848c734ad6328073577f01afe4304"
    )

    assert (
        eddsa.proof_canon
        == b"""{"created":"2023-02-24T23:36:38Z","cryptosuite":"json-eddsa-2022","proofPurpose":"assertionMethod","type":"DataIntegrityProof","verificationMethod":"https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2"}"""  # noqa F501
    )

    assert (
        eddsa.proof_hash.hexdigest()
        == "5b6a51039b9e5a889b6ec4063ce48ad0e5ef6dcedd4c59389146b28092d97774"
    )

    signature = eddsa.sign("z3u2en7t5LR2WtQH5PfFqMqwVHBeXouLzo6haApm8XHqvjxq")

    assert (
        signature
        == "z4J5StJRP9nWYuCGaxgeCWXmmMJvVtLuVKvYv7z9pkGP1nYpaVk5hUaA8cvS37ncvbKWZgUTaSyGcavnVvb1at9kZ"  # noqa F501
    )

    doc_signed = eddsa.signed_doc("z3u2en7t5LR2WtQH5PfFqMqwVHBeXouLzo6haApm8XHqvjxq")

    assert doc_signed == {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://www.w3.org/ns/credentials/examples/v2",
        ],
        "id": "urn:uuid:58172aac-d8ba-11ed-83dd-0b3aef56cc33",
        "type": ["VerifiableCredential", "AlumniCredential"],
        "name": "Alumni Credential",
        "description": "An minimum viable example of an Alumni Credential.",
        "issuer": "https://vc.example/issuers/5678",
        "validFrom": "2023-01-01T00:00:00Z",
        "credentialSubject": {
            "id": "did:example:abcdefgh",
            "alumniOf": "The School of Examples",
        },
        "proof": {
            "type": "DataIntegrityProof",
            "cryptosuite": "json-eddsa-2022",
            "created": "2023-02-24T23:36:38Z",
            "verificationMethod": "https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2",  # noqa F501
            "proofPurpose": "assertionMethod",
            "proofValue": "z4J5StJRP9nWYuCGaxgeCWXmmMJvVtLuVKvYv7z9pkGP1nYpaVk5hUaA8cvS37ncvbKWZgUTaSyGcavnVvb1at9kZ",  # noqa F501
        },
    }

    EdDSA_2022.verify(doc_signed, "z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2")
