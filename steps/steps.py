import json
import asyncio
from behave import given, when, then
from bovine.jsonld import split_into_objects
from vc_di_eddsa import EdDSA_2022


@given("document")
def given_document(context):
    context.document = json.loads(context.text)


@given('Ed25519 Private Key "{secret}"')
def private_key(context, secret):
    context.secret = secret


@given('current time "{timestamp}"')
def step_impl(context, timestamp):
    context.timestamp = timestamp


@when('Signing the document for key "{key}"')
def sign_document(context, key):
    eddsa = EdDSA_2022(context.document, key, context.timestamp)
    context.signed = eddsa.signed_doc(context.secret)


@then("The signed document is")
def check_signed_doc(context):
    expected = json.loads(context.text)
    assert (
        context.signed == expected
    ), f"\n{json.dumps(context.signed)}\n\n\n{json.dumps(expected)}"


@given("The signed document is")
def given_signed_document(context):
    context.signed_doc = json.loads(context.text)


@given("The actor")
def given_actor(context):
    context.actor = json.loads(context.text)


def extract_public_key(parts, verification_method):
    for x in parts:
        if x["id"] == verification_method:
            return x["publicKeyMultibase"]


@when("verifying the document")
def verify_document(context):
    verification_method = context.signed_doc["proof"]["verificationMethod"]

    parts = asyncio.run(split_into_objects(context.actor))

    public_key = extract_public_key(parts, verification_method)

    try:
        EdDSA_2022.verify(context.signed_doc, public_key)
        context.result = True
    except Exception:
        context.result = False


@then("the document is valid")
def document_is_valid(context):
    assert context.result
